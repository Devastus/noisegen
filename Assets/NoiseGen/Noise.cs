﻿using UnityEngine;
using System.Collections;

namespace NoiseGen
{
    public static class Noise
    {
        public enum NoiseType
        {
            Value2D,
            Perlin2D,
            RidgedMultifractal2D,
            Simplex2D
        }

        public enum NormalizeMode
        {
            None,
            Clamp01,
            Local,
            Global
        }

        public enum BlendMode
        {
            Blend,
            Add,
            Subtract,
            Multiply,
            Divide,
            SquarePlus,
            SquareMinus,
            Pow,
            Quadratic
        }

        public enum ColorBlendMode
        {
            Blend,
            Add,
            Subtract,
            Multiply,
            SquarePlus,
            SquareMinus
        }

        public enum FalloffMode
        {
            None,
            Cubic,
            Spherical
        }

        public enum ColorMode
        {
            Base,
            Tint,
            Gradient,
            Overwrite
        }

        public static System.Random prng;

        public static Color[] GenerateColor(NoiseMap noiseMap)
        {
            return GenerateColor(noiseMap.modules, noiseMap.color, noiseMap.size, 1, noiseMap.globalSeed, noiseMap.globalOffset, noiseMap.falloff, noiseMap.curvature, noiseMap.phase, noiseMap.clampOutput);
        }

        /// <summary>
        /// Generate Color array of Noise through the Color Module
        /// </summary>
        /// <param name="modules">NoiseMap noise modules</param>
        /// <param name="color">NoiseMap color module</param>
        /// <param name="size">Size of the noise map</param>
        /// <param name="downscale">Multiplier for resolution downscaling</param>
        /// <param name="globalSeed">Global seed for pseudo-random generation</param>
        /// <param name="globalOffset">Global offset of the NoiseMap</param>
        /// <param name="falloff">Falloff mode</param>
        /// <param name="curvature">Curvature of the falloff map</param>
        /// <param name="phase">Phase of the falloff map</param>
        /// <param name="clampOutput">Clamp output to 0-1?</param>
        /// <returns></returns>
        public static Color[] GenerateColor(NoiseModule[] modules, ColorModule color, int size, int downscale, int globalSeed, Vector2 globalOffset, FalloffMode falloff, float curvature, float phase, bool clampOutput)
        {
            if (downscale < 1) downscale = 1;
            size = size / downscale;
            Color[] noise = new Color[size * size];
            prng = new System.Random(globalSeed);
            Vector2[] octaveOffsets = new Vector2[8];

            //Declare all modifying values
            float amplitude = 1;
            float frequency = 1;
            float signal = 0;
            float weight = 1;
            float value = 0;
            float sum = 0;
            float maxLocalNoiseHeight = float.MinValue;
            float minLocalNoiseHeight = float.MaxValue;
            float maxPossibleHeight = 0;
            int modulesLength = modules.Length;
            Color colorValue;

            //For every octave, change the offset and store it into a variable
            //Get the highest point of multiple amplitudes between octaves
            for (int i = 0; i < 8; i++)
            {
                float offsetX = prng.Next(-100000, 100000) + globalOffset.x;
                float offsetY = prng.Next(-100000, 100000) - globalOffset.y;
                octaveOffsets[i] = new Vector2(offsetX, offsetY);
            }

            //Iterate every pixel
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    colorValue = Color.black;
                    value = 0;
                    sum = 0;
                    //Iterate every active module in NoiseMap
                    for (int m = 0; m < modulesLength; m++)
                    {
                        if (modules[m].active)
                        {
                            //Reset values for every pixel & module
                            amplitude = 1;
                            frequency = 1;
                            signal = 0;
                            weight = 1;

                            if (modules[m].scale <= 0) modules[m].scale = 0.001f;

                            //Iterate octaves for this pixel & module
                            for (int i = 0; i < modules[m].octaves; i++)
                            {
                                //Choose noise type
                                switch (modules[m].noiseType)
                                {
                                    case NoiseType.Value2D:
                                        value = Value(size, x, y, value, ref amplitude, ref frequency, modules[m].persistance, modules[m].lacunarity, modules[m].gain, modules[m].contrast, modules[m].scale / downscale, octaveOffsets[i] + modules[m].offset);
                                        break;
                                    case NoiseType.Perlin2D:
                                        value = Perlin(size, x, y, value, ref amplitude, ref frequency, modules[m].persistance, modules[m].lacunarity, modules[m].gain, modules[m].contrast, modules[m].scale / downscale, octaveOffsets[i] + modules[m].offset);
                                        break;
                                    case NoiseType.Simplex2D:
                                        value = SimplexNoise(size, x, y, value, ref amplitude, ref frequency, modules[m].persistance, modules[m].lacunarity, modules[m].gain, modules[m].contrast, modules[m].scale / downscale, octaveOffsets[i] + modules[m].offset);
                                        break;
                                    case NoiseType.RidgedMultifractal2D:
                                        value = RidgedMultiFractal(size, x, y, value, ref amplitude, ref frequency, ref signal, ref weight, modules[m].persistance, modules[m].lacunarity, modules[m].gain, modules[m].contrast, modules[m].scale / downscale, octaveOffsets[i] + modules[m].offset);
                                        break;
                                }
                            }

                            //If using local normalizing, store the highest and lowest values
                            if (value > maxLocalNoiseHeight)
                            {
                                maxLocalNoiseHeight = value;
                            }
                            else if (value < minLocalNoiseHeight)
                            {
                                minLocalNoiseHeight = value;
                            }

                            //Apply the sum of the module
                            sum = Sum(sum, value, modules[m].blendMode, modules[m].opacity);

                            //Apply falloff
                            sum = Falloff(sum, x, y, size, falloff, curvature, phase);

                            //If normalizing (ie. clamping)
                            if (modules[m].normalizeMode != NormalizeMode.None)
                            {
                                if (modules[m].normalizeMode == NormalizeMode.Clamp01)
                                {
                                    sum = Mathf.Clamp01((sum + 1) / 2);
                                }
                                //Interpolate: lowest stored value -> highest stored value
                                else if (modules[m].normalizeMode == NormalizeMode.Local)
                                {
                                    sum = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, sum);
                                }
                                //Clamp the value: 0 -> highest estimated possible value
                                else if (modules[m].normalizeMode == NormalizeMode.Global)
                                {
                                    float normalizedHeight = (sum + 1) / (2f * maxPossibleHeight / 1.75f);
                                    sum = Mathf.Clamp(normalizedHeight, 0, int.MaxValue);
                                }
                            }

                            if (color.active)
                            {
                                colorValue = new Color(Sum(colorValue.r, sum, modules[m].blendMode, modules[m].R * modules[m].opacity),
                                                       Sum(colorValue.g, sum, modules[m].blendMode, modules[m].G * modules[m].opacity),
                                                       Sum(colorValue.b, sum, modules[m].blendMode, modules[m].B * modules[m].opacity),
                                                       Sum(colorValue.a, sum, modules[m].blendMode, modules[m].A * modules[m].opacity));
                                noise[y * size + x] = SetToColor(colorValue, sum, color);
                            } else
                            {
                                noise[y * size + x] = new Color(sum, sum, sum, sum);
                            }
                                
                        }
                    }
                }
            }
            return noise;
        }

        public static float[,] Generate(NoiseMap noiseMap)
        {
            return Generate(noiseMap.modules, noiseMap.size, 1, noiseMap.globalSeed, noiseMap.globalOffset, noiseMap.falloff, noiseMap.curvature, noiseMap.phase, noiseMap.clampOutput);
        }

        /// <summary>
        /// Generate a float array of Noise
        /// </summary>
        /// <param name="modules">NoiseMap noise modules</param>
        /// <param name="size">Size of the noise map</param>
        /// <param name="downscale">Multiplier for resolution downscaling</param>
        /// <param name="globalSeed">Global seed for pseudo-random generation</param>
        /// <param name="globalOffset">Global offset of the NoiseMap</param>
        /// <param name="falloff">Falloff mode</param>
        /// <param name="curvature">Curvature of the falloff map</param>
        /// <param name="phase">Phase of the falloff map</param>
        /// <param name="clampOutput">Clamp output to 0-1?</param>
        /// <returns></returns>
        public static float[,] Generate(NoiseModule[] modules, int size, int downscale, int globalSeed, Vector2 globalOffset, FalloffMode falloff, float curvature, float phase, bool clampOutput)
        {
            if (downscale < 1) downscale = 1;
            size = size / downscale;
            float[,] noise = new float[size, size];
            prng = new System.Random(globalSeed);
            Vector2[] octaveOffsets = new Vector2[8];

            //Declare all modifying values
            float amplitude = 1;
            float frequency = 1;
            float signal = 0;
            float weight = 1;
            float value = 0;
            float maxLocalNoiseHeight = float.MinValue;
            float minLocalNoiseHeight = float.MaxValue;
            float maxPossibleHeight = 0;
            int modulesLength = modules.Length;

            //For every octave, change the offset and store it into a variable
            //Get the highest point of multiple amplitudes between octaves
            for (int i = 0; i < 8; i++)
            {
                float offsetX = prng.Next(-100000, 100000) + globalOffset.x;
                float offsetY = prng.Next(-100000, 100000) - globalOffset.y;
                octaveOffsets[i] = new Vector2(offsetX, offsetY);
            }

            //Iterate every pixel
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    value = 0;
                    //Iterate every active module in NoiseMap
                    for (int m = 0; m < modulesLength; m++)
                    {
                        if (modules[m].active)
                        {
                            //Reset values for every pixel & module
                            amplitude = 1;
                            frequency = 1;  
                            signal = 0;
                            weight = 1;

                            if (modules[m].scale <= 0) modules[m].scale = 0.001f;

                            //Iterate octaves for this pixel & module
                            for (int i = 0; i < modules[m].octaves; i++)
                            {
                                //Choose noise type
                                switch (modules[m].noiseType)
                                {
                                    case NoiseType.Value2D:
                                        value = Value(size, x, y, value, ref amplitude, ref frequency, modules[m].persistance, modules[m].lacunarity, modules[m].gain, modules[m].contrast, modules[m].scale / downscale, octaveOffsets[i]);
                                        break;
                                    case NoiseType.Perlin2D:
                                        value = Perlin(size, x, y, value, ref amplitude, ref frequency, modules[m].persistance, modules[m].lacunarity, modules[m].gain, modules[m].contrast, modules[m].scale / downscale, octaveOffsets[i]);
                                        break;
                                    case NoiseType.Simplex2D:
                                        value = SimplexNoise(size, x, y, value, ref amplitude, ref frequency, modules[m].persistance, modules[m].lacunarity, modules[m].gain, modules[m].contrast, modules[m].scale / downscale, octaveOffsets[i]);
                                        break;
                                    case NoiseType.RidgedMultifractal2D:
                                        value = RidgedMultiFractal(size, x, y, value, ref amplitude, ref frequency, ref signal, ref weight, modules[m].persistance, modules[m].lacunarity, modules[m].gain, modules[m].contrast, modules[m].scale / downscale, octaveOffsets[i]);
                                        break;
                                }
                            }

                            //If using local normalizing, store the highest and lowest values
                            if (value > maxLocalNoiseHeight)
                            {
                                maxLocalNoiseHeight = value;
                            }
                            else if (value < minLocalNoiseHeight)
                            {
                                minLocalNoiseHeight = value;
                            }

                            //Apply the sum of the module
                            noise[x, y] = Sum(noise[x, y], value, modules[m].blendMode, modules[m].opacity);

                            //Apply falloff
                            noise[x, y] = Falloff(noise[x, y], x, y, size, falloff, curvature, phase);

                            //If normalizing (ie. clamping)
                            if (modules[m].normalizeMode != NormalizeMode.None)
                            {
                                if (modules[m].normalizeMode == NormalizeMode.Clamp01)
                                {
                                    noise[x, y] = Mathf.Clamp01((noise[x, y] +1) / 2);
                                }
                                //Interpolate: lowest stored value -> highest stored value
                                else if (modules[m].normalizeMode == NormalizeMode.Local)
                                {
                                    noise[x, y] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noise[x, y]);
                                }
                                //Clamp the value: 0 -> highest estimated possible value
                                else if (modules[m].normalizeMode == NormalizeMode.Global)
                                {
                                    float normalizedHeight = (noise[x, y] + 1) / (2f * maxPossibleHeight / 1.75f);
                                    noise[x, y] = Mathf.Clamp(normalizedHeight, 0, int.MaxValue);
                                }
                            }
                        }
                    }

                    if (clampOutput) noise[x, y] = Mathf.Clamp01(noise[x, y]);
                }
            }
            return noise;
        }

        #region Old Noise Functions

        public static float[,] Value2D(int mapWidth, int mapHeight, int seed, float scale, float gain, float contrast, Vector2 offset, bool clamp01)
        {

            int hashMask = 255;

            float[,] noiseMap = new float[mapWidth, mapHeight];
            prng = new System.Random(seed);

            int sampleX;
            int sampleY;
            float offsetX = prng.Next(-10000, 10000) + offset.x;
            float offsetY = prng.Next(-10000, 10000) + offset.y;

            scale = scale * 0.5f;
            if (scale <= 0) scale = 0.001f;

            float halfWidth = mapWidth / 2f;
            float halfHeight = mapHeight / 2f;
            float value = 0f;
            float frequency = 1f;

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    sampleX = Mathf.FloorToInt((x - halfWidth + offsetX) / scale * frequency);
                    sampleY = Mathf.FloorToInt((y - halfHeight + offsetY) / scale * frequency);

                    sampleX &= hashMask;
                    sampleY &= hashMask;

                    value = hash[hash[sampleX] + sampleY] * (1f / 255f);
                    value *= gain;

                    if (clamp01)
                    {
                        noiseMap[x, y] = Mathf.Clamp01(Contrast(value, contrast, 4));
                    }
                    else
                    {
                        noiseMap[x, y] = (Contrast(value, contrast, 4) * 2f) - 1f;
                    }
                }
            }

            return noiseMap;
        }

        public static float[,] Perlin2D(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistance, float lacunarity, float gain, float contrast, Vector2 offset, NormalizeMode normalizeMode)
        {
            float[,] noiseMap = new float[mapWidth, mapHeight];
            prng = new System.Random(seed);
            Vector2[] octaveOffsets = new Vector2[octaves];

            float sampleX;
            float sampleY;

            if (scale <= 0) scale = 0.001f;

            float noiseValue = 0;
            float amplitude = 1;
            float frequency = 1;
            float value = 0;
            float maxLocalNoiseHeight = float.MinValue;
            float minLocalNoiseHeight = float.MaxValue;

            float halfWidth = mapWidth / 2f;
            float halfHeight = mapHeight / 2f;
            float maxPossibleHeight = 0;

            for (int i = 0; i < octaves; i++)
            {
                float offsetX = prng.Next(-100000, 100000) + offset.x;
                float offsetY = prng.Next(-100000, 100000) + offset.y;
                octaveOffsets[i] = new Vector2(offsetX, offsetY);

                maxPossibleHeight += amplitude;
                amplitude *= persistance;
            }

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    amplitude = 1;
                    frequency = 1;
                    value = 0;

                    for (int i = 0; i < octaves; i++)
                    {
                        sampleX = (x - halfWidth + octaveOffsets[i].x) / scale * frequency;
                        sampleY = (y - halfHeight + octaveOffsets[i].y) / scale * frequency;

                        noiseValue = Mathf.PerlinNoise(sampleX, sampleY) * 2f - 1f;
                        value += noiseValue * amplitude;
                        value *= gain;
                        value = Contrast(value, contrast, 4);

                        amplitude *= persistance;
                        frequency *= lacunarity;
                    }

                    if (value > maxLocalNoiseHeight)
                    {
                        maxLocalNoiseHeight = value;
                    }
                    else if (value < minLocalNoiseHeight)
                    {
                        minLocalNoiseHeight = value;
                    }
                    noiseMap[x, y] = value;
                }
            }

            if (normalizeMode != NormalizeMode.None)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    for (int x = 0; x < mapWidth; x++)
                    {
                        if(normalizeMode == NormalizeMode.Local)
                        {
                            noiseMap[x, y] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[x, y]);
                        } else if (normalizeMode == NormalizeMode.Global)
                        {
                            float normalizedHeight = (noiseMap[x, y] + 1) / (2f * maxPossibleHeight);
                            noiseMap[x, y] = normalizedHeight;
                        }
                        
                    }
                }
            }

            return noiseMap;
        }

        public static float[,] Simplex2D(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistance, float lacunarity, float gain, float contrast, Vector2 offset, bool clamp01)
        {
            float[,] noiseMap = new float[mapWidth, mapHeight];
            prng = new System.Random(seed);
            Vector2[] octaveOffsets = new Vector2[octaves];

            for (int i = 0; i < octaves; i++)
            {
                float offsetX = prng.Next(-100000, 100000) + offset.x;
                float offsetY = prng.Next(-100000, 100000) + offset.y;
                octaveOffsets[i] = new Vector2(offsetX, offsetY);
            }

            float sampleX;
            float sampleY;

            if (scale <= 0) scale = 0.001f;

            float simplexValue;
            float amplitude;
            float frequency;
            float noiseValue;
            float maxNoiseHeight = float.MinValue;
            float minNoiseHeight = float.MaxValue;

            float halfWidth = mapWidth / 2f;
            float halfHeight = mapHeight / 2f;

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    amplitude = 1;
                    frequency = 1;
                    noiseValue = 0;

                    for (int i = 0; i < octaves; i++)
                    {
                        sampleX = (x - halfWidth + octaveOffsets[i].x) / scale * frequency;
                        sampleY = (y - halfHeight + octaveOffsets[i].y) / scale * frequency;

                        //simplexValue = Simplex.Noise2D(sampleX, sampleY);
                        simplexValue = 1;
                        noiseValue += simplexValue * amplitude;
                        noiseValue *= gain;
                        noiseValue = Contrast(noiseValue, contrast, 4);

                        amplitude *= persistance;
                        frequency *= lacunarity;
                    }

                    if (noiseValue > maxNoiseHeight)
                    {
                        maxNoiseHeight = noiseValue;
                    }
                    else if (noiseValue < minNoiseHeight)
                    {
                        minNoiseHeight = noiseValue;
                    }
                    noiseMap[x, y] = noiseValue;
                }
            }

            if (clamp01)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    for (int x = 0; x < mapWidth; x++)
                    {
                        noiseMap[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[x, y]);
                    }
                }
            }

            return noiseMap;
        }

        public static float[,] RidgedMultiFractal2D(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistance, float lacunarity, float gain, float contrast, Vector2 offset, bool clamp01)
        {
            float[,] noiseMap = new float[mapWidth, mapHeight];
            prng = new System.Random(seed);
            Vector2[] octaveOffsets = new Vector2[octaves];

            for (int i = 0; i < octaves; i++)
            {
                float offsetX = prng.Next(-100000, 100000) + offset.x;
                float offsetY = prng.Next(-100000, 100000) + offset.y;
                octaveOffsets[i] = new Vector2(offsetX, offsetY);
            }

            float sampleX;
            float sampleY;

            if (scale <= 0) scale = 0.001f;

            float value;
            float perlinValue;
            float amplitude;
            float frequency;
            float signal;
            float off;
            float maxNoiseHeight = float.MinValue;
            float minNoiseHeight = float.MaxValue;
            float weight;

            float halfWidth = mapWidth / 2f;
            float halfHeight = mapHeight / 2f;

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    value = 0;
                    amplitude = 1;
                    frequency = 1;
                    weight = 1;
                    off = 1;
                    signal = 0;

                    for (int i = 0; i < octaves; i++)
                    {
                        sampleX = (x - halfWidth + octaveOffsets[i].x) / scale * frequency;
                        sampleY = (y - halfHeight + octaveOffsets[i].y) / scale * frequency;

                        perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2f - 1f;
                        signal += perlinValue * amplitude;
                        signal = Mathf.Abs(signal);
                        signal = off - signal;
                        signal *= signal;
                        signal *= weight;

                        weight = Mathf.Clamp01(signal * gain);
                        value += signal * Mathf.Pow(frequency, -1);
                        amplitude *= persistance;
                        frequency *= lacunarity;
                    }

                    if (signal > maxNoiseHeight)
                    {
                        maxNoiseHeight = value;
                    }
                    else if (signal < minNoiseHeight)
                    {
                        minNoiseHeight = value;
                    }

                    noiseMap[x, y] = Contrast(value, contrast, 4);
                }
            }

            if (clamp01)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    for (int x = 0; x < mapWidth; x++)
                    {
                        noiseMap[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[x, y]);
                    }
                }
            }

            return noiseMap;
        }

        #endregion

        private static Color SetToColor(Color value, float sum, ColorModule colorModule)
        {
            Color baseValue = new Color(sum, sum, sum, sum);
            value *= colorModule.gain;
            value.r = Contrast(value.r, colorModule.contrast, 4);
            value.g = Contrast(value.g, colorModule.contrast, 4);
            value.b = Contrast(value.b, colorModule.contrast, 4);
            value.a = Contrast(value.a, colorModule.contrast, 4);

            if (colorModule.colorMode == ColorMode.Tint)
            {
                value = Color.Lerp(value, value * colorModule.tint, colorModule.tint.a);
            }
            else if (colorModule.colorMode == ColorMode.Gradient)
            {
                Color gradientCol = colorModule.gradient.Evaluate(sum);
                value = Color.Lerp(value, value * gradientCol, gradientCol.a);
            }
            else if (colorModule.colorMode == ColorMode.Overwrite)
            {
                Color gradientCol = colorModule.gradient.Evaluate(sum);
                value = Color.Lerp(value, gradientCol, gradientCol.a);
            } 
            
            value = ColorSum(baseValue, value, colorModule.blendMode, colorModule.opacity);
            value.r = Mathf.Lerp(value.r, colorModule.R.Evaluate(value.r), colorModule.opacity);
            value.g = Mathf.Lerp(value.g, colorModule.G.Evaluate(value.g), colorModule.opacity);
            value.b = Mathf.Lerp(value.b, colorModule.B.Evaluate(value.b), colorModule.opacity);
            value.a = Mathf.Lerp(value.a, colorModule.A.Evaluate(value.a), colorModule.opacity);

            return value;
        }

        private static float Value(float size, int x, int y, float value, ref float amplitude, ref float frequency, float persistance, float lacunarity, float gain, float contrast, float scale, Vector2 offset)
        {
            float noiseValue = 0;

            int sampleX = Mathf.FloorToInt((x - (size * 0.5f) + offset.x) / scale * frequency);
            int sampleY = Mathf.FloorToInt((y - (size * 0.5f) + offset.y) / scale * frequency);
            int hashMask = 255;

            sampleX &= hashMask;
            sampleY &= hashMask;

            noiseValue = hash[hash[sampleX] + sampleY] * (1f/255f) * 2f - 1f;
            value += noiseValue * amplitude;
            value *= gain;
            value = Contrast(value, contrast, 4);

            amplitude *= persistance;
            frequency *= lacunarity;

            return value;
        }

        private static float Perlin(float size, int x, int y, float value, ref float amplitude, ref float frequency, float persistance, float lacunarity, float gain, float contrast, float scale, Vector2 offset)
        {
            float noiseValue = 0;

            float sampleX = (x - (size * 0.5f) + offset.x) / scale * frequency;
            float sampleY = (y - (size * 0.5f) + offset.y) / scale * frequency;

            noiseValue = Mathf.PerlinNoise(sampleX, sampleY) * 2f - 1f;
            value += noiseValue * amplitude;
            value *= gain;
            value = Contrast(value, contrast, 4);

            amplitude *= persistance;
            frequency *= lacunarity;

            return value;
        }

        private static float SimplexNoise(float size, int x, int y, float value, ref float amplitude, ref float frequency, float persistance, float lacunarity, float gain, float contrast, float scale, Vector2 offset)
        {
            float noiseValue = 0;

            float sampleX = (x - (size * 0.5f) + offset.x) / scale * frequency;
            float sampleY = (y - (size * 0.5f) + offset.y) / scale * frequency;

            noiseValue = Simplex.Noise2D(sampleX, sampleY);
            value += noiseValue * amplitude;
            value *= gain;
            value = Contrast(value, contrast, 4);

            amplitude *= persistance;
            frequency *= lacunarity;

            return value;
        }

        private static float RidgedMultiFractal(float size, int x, int y, float value, ref float amplitude, ref float frequency, ref float signal, ref float weight, float persistance, float lacunarity, float gain, float contrast, float scale, Vector2 offset)
        {
            float noiseValue = 0;

            float sampleX = (x - (size * 0.5f) + offset.x) / scale * frequency;
            float sampleY = (y - (size * 0.5f) + offset.y) / scale * frequency;

            noiseValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
            signal += noiseValue * amplitude;
            signal = Mathf.Abs(signal);
            signal = 1 - signal;
            signal *= signal;
            signal *= weight;
            
            value += signal * Mathf.Pow(frequency, -1);
            value = Contrast(value, contrast, 4);
            weight = Mathf.Clamp01(value * gain);
            amplitude *= persistance;
            frequency *= lacunarity;

            return value;
        }

        public static float Sum(float a, float b, BlendMode mode, float opacity)
        {
            float result = 0;

            switch (mode)
            {
                case BlendMode.Blend:
                    result = b;
                    break;
                case BlendMode.Add:
                    result = a + b;
                    break;
                case BlendMode.Subtract:
                    result = a - b;
                    break;
                case BlendMode.Multiply:
                    result = a * b;
                    break;
                case BlendMode.Divide:
                    result = a / b;
                    break;
                case BlendMode.SquarePlus:
                    result = (a * a) + (b * b);
                    break;
                case BlendMode.SquareMinus:
                    result = (a * a) - (b * b);
                    break;
                case BlendMode.Pow:
                    result = Mathf.Pow(a, b);
                    break;
                case BlendMode.Quadratic:
                    result = (a * a) + (a * b) + (b * b);
                    break;
            }

            result = Mathf.Lerp(a, result, opacity);

            return result;
        }

        public static Color ColorSum(Color a, Color b, ColorBlendMode blendMode, float opacity)
        {
            Color result = Color.black;

            switch (blendMode)
            {
                case ColorBlendMode.Blend:
                    result = b;
                    break;
                case ColorBlendMode.Add:
                    result = a + b;
                    break;
                case ColorBlendMode.Subtract:
                    result = a - b;
                    break;
                case ColorBlendMode.Multiply:
                    result = a * b;
                    break;
                case ColorBlendMode.SquarePlus:
                    result = (a * a) + (b * b);
                    break;
                case ColorBlendMode.SquareMinus:
                    result = (a * a) - (b * b);
                    break;
            }

            result = Color.Lerp(a, result, opacity);

            return result;
        }

        public static float Falloff(float value, int x, int y, int size, FalloffMode mode, float curvature, float phase)
        {
            if (mode == FalloffMode.None)
            {
                return value;
            }
            else
            {
                float falloff;

                //If the requested falloff is supposed to be spherical
                if (mode == FalloffMode.Spherical)
                {
                    Vector2 center = new Vector2(size / 2, size / 2);
                    Vector2 pos = new Vector2(x, y);
                    float dstFromCenter = (pos - center).magnitude;

                    falloff = Mathf.Clamp01((dstFromCenter / size) * 2f);

                    value -= Evaluate(falloff, curvature, phase);
                }
                //Otherwise create a squared falloff map
                else if (mode == FalloffMode.Cubic)
                {
                    float i = x / (float)size * 2 - 1;
                    float j = y / (float)size * 2 - 1;

                    falloff = Mathf.Lerp(0, 1, Mathf.Max(Mathf.Abs(i), Mathf.Abs(j)));
                    value -= Evaluate(falloff, curvature, phase);
                }

                return value;
            }
        }

        static float Evaluate(float value, float curvature, float phase)
        {
            float a = curvature;
            float b = phase;

            return Mathf.Pow(value, a) / (Mathf.Pow(value, a) + Mathf.Pow(b - b * value, a));
        }

        static float Contrast(float value, float threshold, int iterations)
        {
            float result = value;
            float c = ((100 + threshold) / 100) * ((100 + threshold) / 100);
            for (int i = 0; i < iterations; i++)
            {
                result = ((result - 0.5f) * c) + 0.5f;
            }

            return Mathf.Clamp(result, -1, 1);
        }

        private static int Mod(int x, int m)
        {
            int a = x % m;
            return a < 0 ? a + m : a;
        }

        static int[] hash = {
        151,160,137, 91, 90, 15,131, 13,201, 95, 96, 53,194,233,  7,225,
        140, 36,103, 30, 69,142,  8, 99, 37,240, 21, 10, 23,190,  6,148,
        247,120,234, 75,  0, 26,197, 62, 94,252,219,203,117, 35, 11, 32,
         57,177, 33, 88,237,149, 56, 87,174, 20,125,136,171,168, 68,175,
         74,165, 71,134,139, 48, 27,166, 77,146,158,231, 83,111,229,122,
         60,211,133,230,220,105, 92, 41, 55, 46,245, 40,244,102,143, 54,
         65, 25, 63,161,  1,216, 80, 73,209, 76,132,187,208, 89, 18,169,
        200,196,135,130,116,188,159, 86,164,100,109,198,173,186,  3, 64,
         52,217,226,250,124,123,  5,202, 38,147,118,126,255, 82, 85,212,
        207,206, 59,227, 47, 16, 58, 17,182,189, 28, 42,223,183,170,213,
        119,248,152,  2, 44,154,163, 70,221,153,101,155,167, 43,172,  9,
        129, 22, 39,253, 19, 98,108,110, 79,113,224,232,178,185,112,104,
        218,246, 97,228,251, 34,242,193,238,210,144, 12,191,179,162,241,
         81, 51,145,235,249, 14,239,107, 49,192,214, 31,181,199,106,157,
        184, 84,204,176,115,121, 50, 45,127,  4,150,254,138,236,205, 93,
        222,114, 67, 29, 24, 72,243,141,128,195, 78, 66,215, 61,156,180,

        151,160,137, 91, 90, 15,131, 13,201, 95, 96, 53,194,233,  7,225,
        140, 36,103, 30, 69,142,  8, 99, 37,240, 21, 10, 23,190,  6,148,
        247,120,234, 75,  0, 26,197, 62, 94,252,219,203,117, 35, 11, 32,
         57,177, 33, 88,237,149, 56, 87,174, 20,125,136,171,168, 68,175,
         74,165, 71,134,139, 48, 27,166, 77,146,158,231, 83,111,229,122,
         60,211,133,230,220,105, 92, 41, 55, 46,245, 40,244,102,143, 54,
         65, 25, 63,161,  1,216, 80, 73,209, 76,132,187,208, 89, 18,169,
        200,196,135,130,116,188,159, 86,164,100,109,198,173,186,  3, 64,
         52,217,226,250,124,123,  5,202, 38,147,118,126,255, 82, 85,212,
        207,206, 59,227, 47, 16, 58, 17,182,189, 28, 42,223,183,170,213,
        119,248,152,  2, 44,154,163, 70,221,153,101,155,167, 43,172,  9,
        129, 22, 39,253, 19, 98,108,110, 79,113,224,232,178,185,112,104,
        218,246, 97,228,251, 34,242,193,238,210,144, 12,191,179,162,241,
         81, 51,145,235,249, 14,239,107, 49,192,214, 31,181,199,106,157,
        184, 84,204,176,115,121, 50, 45,127,  4,150,254,138,236,205, 93,
        222,114, 67, 29, 24, 72,243,141,128,195, 78, 66,215, 61,156,180
        };
    }
}