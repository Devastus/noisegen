﻿using UnityEngine;
using System.Collections;

namespace NoiseGen
{
    public static class TextureGenerator
    {
        public static Texture2D ColorMap(Color[] colorMap, int size)
        {
            if(size >= 1 && size >= 1)
            {
                Texture2D texture = new Texture2D(size, size);
                texture.filterMode = FilterMode.Bilinear;
                texture.wrapMode = TextureWrapMode.Clamp;
                texture.SetPixels(colorMap);
                texture.Apply();
                return texture;
            } else
            {
                return null;
            }
            
        }

        public static Texture2D Color32Map(Color32[] colorMap, int size)
        {
            if (size >= 1 && size >= 1)
            {
                Texture2D texture = new Texture2D(size, size);
                texture.filterMode = FilterMode.Bilinear;
                texture.wrapMode = TextureWrapMode.Clamp;
                texture.SetPixels32(colorMap);
                texture.Apply();
                return texture;
            }
            else
            {
                return null;
            }

        }

        public static Texture2D HeightMap(float[,] heightMap)
        {
            int size = heightMap.GetLength(0);

            Color[] colorMap = new Color[size * size];
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    colorMap[y * size + x] = Color.Lerp(Color.black, Color.white, heightMap[x, y]);
                }
            }

            return ColorMap(colorMap, size);
        }

        public static void SaveTextureToFile(Texture2D texture, string filePath)
        {
            System.IO.File.WriteAllBytes(filePath, texture.EncodeToPNG());
        }

        public static Texture2D LoadTextureFromFile(string filePath, bool markNonReadable)
        {
            byte[] bytes = System.IO.File.ReadAllBytes(filePath);
            Texture2D tex = new Texture2D(1, 1);
            tex.LoadImage(bytes, markNonReadable);
            return tex;
        }
    }
}