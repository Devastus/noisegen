﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace NoiseGen
{
    [CustomEditor(typeof(NoiseMap))]
    public class NoiseMapEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            if(GUILayout.Button("Open NoiseGen Editor"))
            {
                NoiseGenEditorWindow.ShowWindow(AssetDatabase.GetAssetPath((NoiseMap)target));
            }
        }
    }
}