﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace NoiseGen
{
    public class NoiseGenEditorWindow : EditorWindow
    {
        public static NoiseGenEditorWindow window;
        private static Vector2 windowMinSize { get { return new Vector2(430, 880); } }
        [SerializeField]
        public static NoiseMap target;

        private static bool[] activeModules = { false, false, false, false, false, false };
        private static bool activeColor = false;
        private static int size = 128, globalSeed = 0;
        private static float curvature = 3f, phase = 2.2f;
        private static Vector2 globalOffset = Vector2.zero;
        private static bool clampOutput = true;
        private static Texture2D previewTexture;
        private static int prevTexDownscale = 1;
        private static bool updatePrev = true;
        private static float downscaledModuleScale;

        Noise.FalloffMode falloff;
        int noiseModuleAmount = 0;

        Vector2 moduleScroll;
        GUIStyle modHeaderOffStyle = new GUIStyle();
        GUIStyle modHeaderOnStyle = new GUIStyle();
        GUIStyle moduleOffStyle = new GUIStyle();
        GUIStyle moduleOnStyle = new GUIStyle();
        GUIStyle prevAreaText = new GUIStyle();
        GUIStyle prevAreaTextDisabled = new GUIStyle();
        Color defaultColor;

        [MenuItem("Tools/NoiseGen Editor")]
        public static void ShowWindowFromMenu()
        {
            ShowWindow(null);
        }

        public static void ShowWindow(string assetPath)
        {
            SetTarget(assetPath);

            window = (NoiseGenEditorWindow)EditorWindow.GetWindow(typeof(NoiseGenEditorWindow));
            window.titleContent = new GUIContent("NoiseGen");
            window.minSize = windowMinSize;

            SetupEditor();
        }

        private static void SetTarget(string assetPath)
        {
            EditorPrefs.SetString("NoiseGen", assetPath);
        }

        private static void GetTarget()
        {
            if (EditorPrefs.HasKey("NoiseGen"))
            {
                target = AssetDatabase.LoadAssetAtPath<NoiseMap>(EditorPrefs.GetString("NoiseGen"));
                if (target == null)
                    target = NoiseMap.CreateInstance<NoiseMap>();
            } else
            {
                target = NoiseMap.CreateInstance<NoiseMap>();
            }
        }

        private static void SetupEditor()
        {
            GetTarget();

            if (target != null)
            {
                int length = target.modules.Length;
                size = target.size;
                globalSeed = target.globalSeed;
                clampOutput = target.clampOutput;
                for (int i = 0; i < length; i++)
                {
                    activeModules[i] = target.modules[i].active;
                }
                activeColor = target.color.active;

                window.RedrawAuto();
            }
        }

        void OnGUI()
        {
            if(target != null && window != null)
            {
                Event evt = Event.current;

                //Style declarations
                moduleOffStyle.normal.background = GUIStyleTex(1, 1, new Color(0.78f, 0.78f, 0.78f));
                moduleOnStyle.normal.background = GUIStyleTex(1, 1, new Color(0.67f, 0.75f, 0.82f));
                modHeaderOffStyle.normal.background = GUIStyleTex(1, 1, new Color(0.6f, 0.6f, 0.6f));
                modHeaderOnStyle.normal.background = GUIStyleTex(1, 1, new Color(0.66f, 0.82f, 1f));
                prevAreaText.normal.textColor = Color.white;
                prevAreaTextDisabled.normal.textColor = Color.gray;
                defaultColor = GUI.backgroundColor;

                //Preview picture area
                Rect prevTexArea = new Rect(0, 0, position.width, position.height / 2.75f);
                float prevTexSize = Mathf.Min(prevTexArea.width, prevTexArea.height);
                Rect prevTextureRect = new Rect((prevTexArea.width - prevTexSize) / 2f, (prevTexArea.height - prevTexSize) / 2f, prevTexSize, prevTexSize);
                Rect prevTargetName = new Rect(5, 2, 50f, 20f);
                Rect prevUpdateTextArea = new Rect(position.width - 55f, 2f, 50f, 20f);
                Rect prevDownscaleTextArea = new Rect(position.width - 55f, 20f, 50f, 20f);

                EditorGUI.DrawRect(prevTexArea, Color.black);
                if (previewTexture != null) {
                    EditorGUI.DrawPreviewTexture(prevTextureRect, previewTexture);
                } else
                {
                    RedrawAuto();
                }
                prevAreaText.alignment = TextAnchor.UpperLeft;
                EditorGUI.DropShadowLabel(prevTargetName, target.name, prevAreaText);
                prevAreaTextDisabled.alignment = TextAnchor.UpperRight;
                prevAreaText.alignment = TextAnchor.UpperRight; 
                EditorGUI.DropShadowLabel(prevDownscaleTextArea, "1:" + prevTexDownscale, prevAreaText);
                EditorGUI.DropShadowLabel(prevUpdateTextArea, "Update", updatePrev ? prevAreaText : prevAreaTextDisabled);

                if (evt.type == EventType.MouseDown)
                {
                    Vector2 mousePos = evt.mousePosition;
                    if (prevDownscaleTextArea.Contains(mousePos))
                    {
                        GenericMenu menu = new GenericMenu();
                        menu.AddItem(new GUIContent("1:1"), false, ChangeDownscale, 1);
                        menu.AddItem(new GUIContent("1:2"), false, ChangeDownscale, 2);
                        menu.AddItem(new GUIContent("1:4"), false, ChangeDownscale, 4);
                        menu.AddItem(new GUIContent("1:8"), false, ChangeDownscale, 8);
                        menu.AddItem(new GUIContent("1:16"), false, ChangeDownscale, 16);
                        menu.DropDown(prevDownscaleTextArea);
                        evt.Use();
                    }
                    if (prevUpdateTextArea.Contains(mousePos))
                    {
                        updatePrev = !updatePrev;
                        evt.Use();
                    }
                    if (prevTextureRect.Contains(mousePos))
                    {
                        RedrawEvent();
                        evt.Use();
                    }
                }
                GUILayout.Space(prevTexArea.height + 4);

                //EditorWindow controls
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUIUtility.labelWidth = 70f;
                    falloff = (Noise.FalloffMode)EditorGUILayout.EnumPopup("Falloff", falloff);
                    target.falloff = falloff;
                    curvature = Mathf.Clamp(EditorGUILayout.FloatField("Curvature", curvature),0,10);
                    target.curvature = curvature;
                    phase = Mathf.Clamp(EditorGUILayout.FloatField("Phase", phase),0,10);
                    target.phase = phase;
                }
                EditorGUILayout.EndHorizontal();   
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUIUtility.labelWidth = 100f;
                    clampOutput = EditorGUILayout.Toggle("Clamp Output", clampOutput);
                    target.clampOutput = clampOutput;
                    globalSeed = Mathf.Clamp(EditorGUILayout.IntField("Seed", globalSeed), -10000, 10000);
                    target.globalSeed = globalSeed;
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUIUtility.labelWidth = 80f;
                    size = EditorGUILayout.IntField("Size", size);
                    size = Mathf.Clamp(size, 2, 4096);
                    target.size = size;
                    globalOffset = EditorGUILayout.Vector2Field(GUIContent.none, globalOffset);
                    target.globalOffset = globalOffset;
                    EditorGUIUtility.labelWidth = 0f;
                }
                EditorGUILayout.EndHorizontal();
                if (EditorGUI.EndChangeCheck())
                {
                    EditorUtility.SetDirty(target);
                    RedrawAuto();
                }

                EditorGUIUtility.labelWidth = 100f;
                noiseModuleAmount = 6;

                //Horizontal Modules section with scrollbar
                moduleScroll = EditorGUILayout.BeginScrollView(moduleScroll, GUILayout.Width(position.width), GUILayout.Height(268));
                {
                    EditorGUI.BeginChangeCheck();
                    EditorGUILayout.BeginHorizontal();
                    {
                        for (int i = 0; i < noiseModuleAmount; i++)
                        {
                            EditorGUILayout.BeginVertical(activeModules[i] == true ? modHeaderOnStyle : modHeaderOffStyle, GUILayout.Width(276), GUILayout.Height(250));
                            activeModules[i] = EditorGUILayout.BeginToggleGroup("Module " + (i + 1), activeModules[i]);

                            EditorGUILayout.BeginVertical(activeModules[i] == true ? moduleOnStyle : moduleOffStyle, GUILayout.Width(276));
                            target.modules[i].active = activeModules[i];
                            DrawNoiseModule(target.modules[i]);
                            EditorGUILayout.EndVertical();

                            EditorGUILayout.EndToggleGroup();
                            EditorGUILayout.EndVertical();
                            EditorGUILayout.Space();
                        }
                    }

                    EditorGUILayout.EndHorizontal();
                    if (EditorGUI.EndChangeCheck())
                    {
                        EditorUtility.SetDirty(target);
                        RedrawAuto();
                    }
                }
                EditorGUILayout.EndScrollView();
                EditorGUILayout.Space();

                //Color module
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.BeginVertical(activeColor == true ? modHeaderOnStyle : modHeaderOffStyle, GUILayout.Height(204));
                    activeColor = EditorGUILayout.BeginToggleGroup("Color", activeColor);
                    target.color.active = activeColor;
                    EditorGUILayout.BeginVertical(activeColor == true ? moduleOnStyle : moduleOffStyle );

                    DrawColorModule(target.color);

                    EditorGUILayout.EndVertical();
                    EditorGUILayout.EndToggleGroup();
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();
                if (EditorGUI.EndChangeCheck())
                {
                    EditorUtility.SetDirty(target);
                    RedrawAuto();
                }

                //Generate Texture button with specified color
                if (GUILayout.Button("Save Texture To File"))
                {
                    string path = EditorUtility.SaveFilePanel("Save Texture to...", "", "", "png");
                    if (path.Length > 0)
                    {
                        if (target.color.active)
                        {
                            TextureGenerator.SaveTextureToFile(TextureGenerator.ColorMap(Noise.GenerateColor(target), target.size), path);
                        } else
                        {
                            TextureGenerator.SaveTextureToFile(TextureGenerator.HeightMap(Noise.Generate(target)), path);
                        }
                        AssetDatabase.Refresh();
                    }
                }
            }
        }

        //Changes the resolution of the preview texture
        void ChangeDownscale(object downscale)
        {
            if (prevTexDownscale != (int)downscale)
            {
                prevTexDownscale = (int)downscale;
                RedrawEvent();
            }
        }

        void Update()
        {
            if(target == null || window == null)
            {
                ShowWindow(EditorPrefs.GetString("NoiseGen"));
            }
        }

        //Redraw the texture and the editor window automatically when Update is on
        private void RedrawAuto()
        {
            if(updatePrev)
            {
                Redraw();
                Repaint();
            }
        }

        //Redraws the texture and the editor through an event regardless of update state
        private void RedrawEvent()
        {
            Redraw();
            Repaint();
        }

        //Handles redrawing
        private void Redraw()
        {
            if (previewTexture) DestroyImmediate(previewTexture);
            if (target.color.active)
            {
                previewTexture = TextureGenerator.ColorMap(Noise.GenerateColor(target.modules, target.color, target.size, prevTexDownscale, target.globalSeed, target.globalOffset, target.falloff, target.curvature, target.phase, target.clampOutput), target.size / prevTexDownscale);
            }
            else
            {
                previewTexture = TextureGenerator.HeightMap(Noise.Generate(target.modules, target.size, prevTexDownscale, target.globalSeed, target.globalOffset, target.falloff, target.curvature, target.phase, target.clampOutput));
            }
        }

        //Draw the module's properties
        private void DrawNoiseModule(NoiseModule module)
        {
            EditorGUIUtility.labelWidth = 100f;
            module.noiseType = (Noise.NoiseType)EditorGUILayout.EnumPopup(module.noiseType);
            module.seed = globalSeed;

            module.scale = EditorGUILayout.FloatField("Scale", module.scale);
            //downscaledModuleScale = module.scale / prevTexDownscale;

            module.octaves = EditorGUILayout.IntSlider("Octaves", module.octaves, 1, 8);
            module.persistance = EditorGUILayout.Slider("Persistance", module.persistance, 0, 1);
            module.lacunarity = EditorGUILayout.Slider("Lacunarity", module.lacunarity, 1, 6);
            module.gain = EditorGUILayout.Slider("Gain", module.gain, 0, 4);
            module.contrast = EditorGUILayout.Slider("Contrast", module.contrast, -50, 50);

            EditorGUILayout.BeginHorizontal(GUILayout.Width(276));
            EditorGUILayout.PrefixLabel("Offset");
            module.offset = EditorGUILayout.Vector2Field(GUIContent.none, module.offset);
            EditorGUILayout.EndHorizontal();

            module.blendMode = (Noise.BlendMode)EditorGUILayout.EnumPopup("Blend Mode", module.blendMode);
            module.opacity = EditorGUILayout.Slider("Opacity", module.opacity, 0, 1);
            module.normalizeMode = (Noise.NormalizeMode)EditorGUILayout.EnumPopup(new GUIContent("Normalize Mode", "None = unclamped, Clamp01 = resize and clamp to 0-1, Local = min/max interpolation (will produce seams in tiling), Global = clamp to highest amplitude estimation"), module.normalizeMode);
            EditorGUILayout.BeginHorizontal(GUILayout.Width(276));;
            EditorGUIUtility.labelWidth = 15f;
            GUI.backgroundColor = (target.color.active) ? new Color(1f, 0.25f, 0.25f) : Color.gray;
            module.R = EditorGUILayout.FloatField("R", module.R, GUILayout.Width(65));
            module.R = Mathf.Clamp01(module.R);
            GUI.backgroundColor = (target.color.active) ? new Color(0.25f, 1f, 0.25f) : Color.gray;
            module.G = EditorGUILayout.FloatField("G", module.G, GUILayout.Width(65));
            module.G = Mathf.Clamp01(module.G);
            GUI.backgroundColor = (target.color.active) ? new Color(0.25f, 0.25f, 1f): Color.gray;
            module.B = EditorGUILayout.FloatField("B", module.B, GUILayout.Width(65));
            module.B = Mathf.Clamp01(module.B);
            GUI.backgroundColor = (target.color.active) ? defaultColor : Color.gray;
            module.A = EditorGUILayout.FloatField("A", module.A, GUILayout.Width(65));
            module.A = Mathf.Clamp01(module.A);
            GUI.backgroundColor = defaultColor;
            EditorGUIUtility.labelWidth = 100f;
            EditorGUILayout.EndHorizontal();
        }

        private void DrawColorModule(ColorModule color)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical(GUILayout.Width(position.width / 2));
            EditorGUIUtility.labelWidth = 100f;
            color.colorMode = (Noise.ColorMode)EditorGUILayout.EnumPopup(color.colorMode);
            color.gain = EditorGUILayout.Slider("Gain", color.gain, 0, 4);
            color.contrast = EditorGUILayout.Slider("Contrast", color.contrast, -50, 50);
            color.blendMode = (Noise.ColorBlendMode)EditorGUILayout.EnumPopup("Blend Mode", color.blendMode);
            color.opacity = EditorGUILayout.Slider("Opacity", color.opacity, 0, 1);
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(GUILayout.Width(position.width/2 - 8));
            EditorGUIUtility.labelWidth = 100f;
            if (color.colorMode == Noise.ColorMode.Tint && target.color.active)
            {
                color.tint = EditorGUILayout.ColorField(color.tint);
            }
            else if (color.colorMode == Noise.ColorMode.Gradient || color.colorMode == Noise.ColorMode.Overwrite && target.color.active)
            {
                SerializedObject serializedGradient = new SerializedObject(target);
                SerializedProperty gradient = serializedGradient.FindProperty("color").FindPropertyRelative("gradient");
                EditorGUILayout.PropertyField(gradient, GUIContent.none, true);
                serializedGradient.ApplyModifiedProperties();
            }
            else
            {
                GUILayout.Space(18);
            }
 
            GUI.backgroundColor = (target.color.active) ? new Color(1f, 0.25f, 0.25f) : Color.gray;
            color.R = EditorGUILayout.CurveField(color.R);
            GUI.backgroundColor = (target.color.active) ? new Color(0.25f, 1f, 0.25f) : Color.gray;
            color.G = EditorGUILayout.CurveField(color.G);
            GUI.backgroundColor = (target.color.active) ? new Color(0.25f, 0.25f, 1f) : Color.gray;
            color.B = EditorGUILayout.CurveField(color.B);
            GUI.backgroundColor = (target.color.active) ? defaultColor : Color.gray;
            color.A = EditorGUILayout.CurveField(color.A);
            GUI.backgroundColor = defaultColor;
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
        }

        //Make texture for GUI
        private Texture2D GUIStyleTex(int width, int height, Color col)
        {
            Color[] pix = new Color[width * height];

            for (int i = 0; i < pix.Length; i++)
                pix[i] = col;

            Texture2D result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();

            return result;
        }

        //Currently not working as intended
        public void SetTextureImporterFormat(Texture2D texture, bool isReadable)
        {
            if (null == texture) return;

            string assetPath = AssetDatabase.GetAssetPath(texture);
            var tImporter = AssetImporter.GetAtPath(assetPath) as TextureImporter;
            if (tImporter != null)
            {
                tImporter.textureType = TextureImporterType.Default;

                tImporter.isReadable = isReadable;

                AssetDatabase.ImportAsset(assetPath);
                AssetDatabase.Refresh();
            }
        }
    }

}
