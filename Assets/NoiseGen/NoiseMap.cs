﻿using UnityEngine;
using System.Collections;


namespace NoiseGen
{
    [CreateAssetMenu(menuName = "NoiseGen/Noise Map")]
    [System.Serializable]
    public class NoiseMap : ScriptableObject
    {
        [SerializeField]
        public Noise.FalloffMode falloff;
        [SerializeField]
        public float curvature = 3f;
        [SerializeField]
        public float phase = 2.2f;
        [SerializeField]
        public int size = 128;
        [SerializeField]
        public int globalSeed;
        [SerializeField]
        public Vector2 globalOffset;
        [SerializeField]
        public bool clampOutput;
        [SerializeField]
        public NoiseModule[] modules;
        [SerializeField]
        public ColorModule color;

        void OnEnable()
        {
            if (modules == null)
            {
                modules = new NoiseModule[6];
                for (int i = 0; i < modules.Length; i++)
                {
                    modules[i] = new NoiseModule(false, Noise.NoiseType.Value2D, 0, 32f, 2, 0.5f, 2f, 1f, 0f, Vector2.zero, Noise.BlendMode.Blend, 1f, Noise.NormalizeMode.None, 1f, 1f, 1f, 1f);
                }
            }
            if(color == null) color = new ColorModule(false, Noise.ColorMode.Base, 1f, 0f, Noise.ColorBlendMode.Blend, 1f, Color.white, new Gradient(), AnimationCurve.Linear(0,0,1,1), AnimationCurve.Linear(0, 0, 1, 1), AnimationCurve.Linear(0, 0, 1, 1), AnimationCurve.Linear(0, 0, 1, 1));
        }
    }

    [System.Serializable]
    public class NoiseModule
    {
        [SerializeField]
        public bool active = false;
        [SerializeField]
        public Noise.NoiseType noiseType;
        [SerializeField]
        public int seed = 0;
        [SerializeField]
        public float scale = 32f;
        [Range(1, 8)]
        [SerializeField]
        public int octaves = 2;
        [Range(0, 1)]
        [SerializeField]
        public float persistance = 0.5f;
        [Range(1, 6)]
        [SerializeField]
        public float lacunarity = 2f;
        [Range(0, 4)]
        [SerializeField]
        public float gain = 1f;
        [Range(-50, 50)]
        [SerializeField]
        public float contrast = 0;
        [SerializeField]
        public Vector2 offset;
        [SerializeField]
        public Noise.BlendMode blendMode;
        [Range(0, 1)]
        [SerializeField]
        public float opacity = 1f;
        [SerializeField]
        public Noise.NormalizeMode normalizeMode;
        [SerializeField]
        public float R = 1f;
        [SerializeField]
        public float G = 1f;
        [SerializeField]
        public float B = 1f;
        [SerializeField]
        public float A = 1f;

        public NoiseModule(bool active, Noise.NoiseType noiseType, int seed, float scale, int octaves, float persistance, float lacunarity, float gain, float contrast, Vector2 offset, Noise.BlendMode blendMode, float opacity, Noise.NormalizeMode normalizeMode, float R, float G, float B, float A)
        {
            this.active = active;
            this.noiseType = noiseType;
            this.seed = seed;
            this.scale = scale;
            this.octaves = octaves;
            this.persistance = persistance;
            this.lacunarity = lacunarity;
            this.gain = gain;
            this.contrast = contrast;
            this.offset = offset;
            this.blendMode = blendMode;
            this.opacity = opacity;
            this.normalizeMode = normalizeMode;
            this.R = R;
            this.G = G;
            this.B = B;
            this.A = A;
        }
    }

    [System.Serializable]
    public class ColorModule
    {
        [SerializeField]
        public bool active = false;
        [SerializeField]
        public Noise.ColorMode colorMode;
        [Range(0, 4)]
        [SerializeField]
        public float gain = 1f;
        [Range(-50, 50)]
        [SerializeField]
        public float contrast = 0f;
        [SerializeField]
        public Noise.ColorBlendMode blendMode;
        [Range(0, 1)]
        [SerializeField]
        public float opacity = 1f;
        [SerializeField]
        public Color tint;
        [SerializeField]
        public Gradient gradient;
        [SerializeField]
        public AnimationCurve R;
        [SerializeField]
        public AnimationCurve G;
        [SerializeField]
        public AnimationCurve B;
        [SerializeField]
        public AnimationCurve A;

        public ColorModule(bool active, Noise.ColorMode colorMode, float gain, float contrast, Noise.ColorBlendMode blendMode, float opacity, Color tint, Gradient gradient, AnimationCurve R, AnimationCurve G, AnimationCurve B, AnimationCurve A)
        {
            this.active = active;
            this.colorMode = colorMode;
            this.gain = gain;
            this.contrast = contrast;
            this.blendMode = blendMode;
            this.opacity = opacity;
            this.tint = tint;
            this.gradient = gradient;
            this.R = R;
            this.G = G;
            this.B = B;
            this.A = A;
        }
    }
}

